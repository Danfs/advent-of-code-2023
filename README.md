# Advent of Code 2023

## Description

The codes I did to complete the Advent of Code 2023.

Me and my friend Vinícius decided to challenge ourselves into using a language that we have never touched before, so we could learn something new.
From a list of 8 languages, we decided (randomly) to use Rust, and we hope to try other languages on the years to come/when doing previous years.

## Usage

This project was designed such that each challenge has its own binary.

For example, to run the first problem of the first day, you run `cargo run --bin aoc_01a`.

## Completion List

* [x] 01a
* [x] 01b
* [x] 02a
* [x] 02b
* [ ] 03a
* [ ] 03b
* [x] 04a
* [x] 04b
* [x] 05a
* [ ] 05b
* [x] 06a
* [x] 06b
* [x] 07a
* [x] 07b
* [x] 08a
* [ ] 08b
* [x] 09a
* [x] 09b
* [x] 10a
* [ ] 10b
* [x] 11a
* [x] 11b
* [ ] 12a
* [ ] 12b
* [ ] 13a
* [ ] 13b
* [x] 14a
* [x] 14b
* [x] 15a
* [x] 15b
* [x] 16a
* [x] 16b
* [ ] 17a
* [ ] 17b
* [ ] 18a
* [ ] 18b
* [x] 19a
* [ ] 19b
* [ ] 20a
* [ ] 20b
* [ ] 21a
* [ ] 21b
* [ ] 22a
* [ ] 22b
* [ ] 23a
* [ ] 23b
* [ ] 24a
* [ ] 24b
* [ ] 25a
* [ ] 25b
