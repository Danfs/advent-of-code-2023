static NUMBERS: [(&str, usize); 9] = [
    ("one",   1),
    ("two",   2),
    ("three", 3),
    ("four",  4),
    ("five",  5),
    ("six",   6),
    ("seven", 7),
    ("eight", 8),
    ("nine",  9),
];

fn get_number(s: &String) -> usize {
    let mut s = s.clone();
    while s != "" {
        let fst_char = s.chars().next().unwrap();
        if fst_char.is_ascii_digit() {
            return fst_char.to_digit(10).unwrap() as usize;
        }
        for (txt, n) in NUMBERS {
            if s.starts_with(txt) {
                return n;
            }
        }
        s = s[1..].to_string();
    }
    11
}

fn read_line(s: &str) -> usize {
    let mut s = s.to_string();

    let fst = get_number(&s);
    let mut lst = fst;
    s = s[1..].to_string();

    while !s.is_empty() {
        let b = get_number(&s);
        if b != 11 {
            lst = b;
        }
        s = s[1..].to_string();
    }
    fst*10+lst
}

fn main() {
    let input = include_str!("../../inputs/01.txt");

    let acc: usize = input.lines().map(read_line).sum();

    println!("{}", acc);
}
