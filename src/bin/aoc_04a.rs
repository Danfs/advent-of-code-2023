use std::collections::HashSet;

fn read_line(line: &str) -> usize {
    let (_, numbers) = line.split_once(':').unwrap();
    let (prize_nums, my_nums) = numbers.split_once('|').unwrap();

    let my_nums:    HashSet<usize> = HashSet::from_iter(my_nums   .split(' ').filter_map(|x| x.parse::<usize>().ok()));
    let prize_nums: HashSet<usize> = HashSet::from_iter(prize_nums.split(' ').filter_map(|x| x.parse::<usize>().ok()));
    let amount = prize_nums.intersection(&my_nums).count() as u32;
    if amount == 0 {
        0
    } else {
        2usize.pow(amount-1)
    }
}

fn main() {
    let input = include_str!("../../inputs/04.txt");

    let result: usize = input.lines().map(read_line).sum();
    println!("{}", result);
}
