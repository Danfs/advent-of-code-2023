use std::iter::{once, repeat_with};

fn process_lines(line: &str) -> isize {
    let numbers = line.split_ascii_whitespace()
        .map(|x| x.parse::<isize>().unwrap())
        .collect::<Vec<_>>();
    let len = numbers.len();

    let mut sequences = Vec::from_iter(
        once(numbers).chain(repeat_with(Vec::new).take(len-1))
    );

    let mut idx: usize = 0;
    loop {
        let seq = sequences.get(idx).unwrap().to_owned();
        if seq.iter().all(|x| *x == 0) {
            break;
        }

        idx += 1;
        let new_seq = sequences.get_mut(idx).unwrap();

        for i in 1..seq.len() {
            new_seq.push(seq[i]-seq[i-1]);
        }
    }
    sequences.iter().filter(|x| !x.is_empty())
        .map(|x| x.last().unwrap())
        .sum::<isize>()
}

fn main() {
    let input = include_str!("../../inputs/09.txt").lines();

    let result: isize = input.map(process_lines).sum();

    println!("{}", result);
}