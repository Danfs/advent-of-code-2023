fn read_line(s: &str) -> u32 {
    let (game, results) = s.split_once(": ").unwrap();
    let (_, id) = game.split_once(' ').unwrap();

    for result in results.split("; ") {
        for ball in result.split(", ") {
            let (n, colour) = ball.split_once(' ').unwrap();
            let n: u32 = n.parse().unwrap();
            match colour {
                "red"   => if n > 12 { return 0}
                "green" => if n > 13 { return 0}
                "blue"  => if n > 14 { return 0}
                _       => {}
            }
        }
    }
    id.parse::<u32>().unwrap()
}

fn main() {
    let input = include_str!("../../inputs/02.txt");

    let result = input.lines().fold(0, |acc, x| acc+read_line(x));

    println!("{}", result)
}
