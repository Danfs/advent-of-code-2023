fn calculate(time: usize, dist: usize) -> usize {
    (1..time)
        .filter(|x| (x * (time-x)) > dist)
        .map(|x| time+1 - (2*x))
        .next().unwrap_or(0)
}

fn main() {
    let mut input = include_str!("../../inputs/06.txt").lines();

    let times = input.next().unwrap()
        .split_whitespace().skip(1)
        .filter_map(|x| x.parse::<usize>().ok());
    let dists = input.next().unwrap()
        .split_whitespace().skip(1)
        .filter_map(|x| x.parse::<usize>().ok());

    let result = times.zip(dists)
        .map(|(x, y)| calculate(x,y))
        .fold(1, |acc, x| acc*x);
    println!("{}", result);
}
