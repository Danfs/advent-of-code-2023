use std::iter;

fn roll_north(grid: &mut Vec<Vec<char>>) {
    let lines = grid.len();
    let colus = grid.first().unwrap().len();

    let mut vec = Vec::from_iter(iter::repeat(0usize).take(colus));
    for line in 0..lines {
        for col in 0..colus {
            let free_line = vec.get_mut(col).unwrap();
            match grid[line][col] {
                '.' => {}
                'O' => {
                    grid[line][col] = '.';
                    grid[*free_line][col] = 'O';
                    *free_line += 1;
                }
                '#' => {
                    *free_line = line+1;
                }
                _ => panic!(),
            };
        }
    }
}

fn calculate_load(grid: &Vec<Vec<char>>) -> usize {
    let calculate_line_load = |weight: usize, line: &Vec<char>| {
        let amount = line.iter().filter(|c| **c == 'O').count();
        amount*weight
    };

    let len = grid.len();
    grid.iter().enumerate().map(|(idx, line)| calculate_line_load(len-idx, line)).sum()
}

fn main() {
    let mut input = include_str!("../../inputs/14.txt").lines().map(
        |x| x.chars().collect::<Vec<_>>()
    ).collect::<Vec<_>>();
    let input = input.as_mut();

    roll_north(input);

    let result = calculate_load(&input);
    println!("{}", result)
}
