fn read_line(s: &str) -> u32 {
    let mut digits = s.chars().filter(|x| x.is_ascii_digit());

    let fst = digits.next().unwrap();
    let lst = digits.last().unwrap_or(fst);

    let fst = fst.to_digit(10).unwrap();
    let lst = lst.to_digit(10).unwrap();

    fst*10 + lst
}


fn main() {
    let input = include_str!("../../inputs/01.txt");

    let acc = input.lines().fold(0, |acc, x| acc+read_line(x));

    println!("{}", acc);
}
