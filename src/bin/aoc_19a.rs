use std::collections::hash_map::{RandomState, HashMap};

struct Part {
    x: u32,
    m: u32,
    a: u32,
    s: u32,
}

struct Rule {
    attr: char,
    compare: char,
    limiar_value: u32,
    destination: String
}

struct Workflow {
    rules: Vec<Rule>,
    default: String
}

fn apply_rule(p: &Part, r: &Rule) -> bool {
    let value = match r.attr {
        'x' => p.x,
        'm' => p.m,
        'a' => p.a,
        's' => p.s,
        _ => panic!()
    };

    match r.compare {
        '<' => value < r.limiar_value,
        '>' => value > r.limiar_value,
        _ => panic!()
    }
}

fn main() {
    let mut input = include_str!("../../inputs/19.txt").lines();

    let workflow_input = input.by_ref().take_while(|l| !l.is_empty());

    let read_rule = |rule: &str| {
        let (attr, rest) = rule.split_at(1);
        let (signal, rest) = rest.split_at(1);
        let (value, destination) = rest.split_once(':').unwrap();

        return Rule {
            attr: attr.chars().next().unwrap(),
            compare: signal.chars().next().unwrap(),
            limiar_value: value.parse().unwrap(),
            destination: destination.to_string()
        };
    };

    let line_to_workflow = |line: &str| {
        let (name, rules) = line.split_once('{').unwrap();

        let mut raw_rules = rules.split(',').peekable();

        let mut builded_rules: Vec<Rule> = Vec::new();
        let mut element;
        loop {
            element = raw_rules.next().unwrap();
            if raw_rules.peek().is_none() {
                break;
            }
            builded_rules.push(read_rule(element));
        }

        return (name.to_string(), Workflow { rules: builded_rules, default: element.trim_end_matches('}').to_string() })
    };

    let flows: HashMap<String, Workflow, RandomState> = HashMap::from_iter(
        workflow_input.map(line_to_workflow)
    );

    let line_to_part = |line: &str| {
        let mut attrs = line.trim_end_matches('}').split(',')
            .map(|x| x.split_once('=').unwrap().1.parse::<u32>().unwrap());

        Part {
            x: attrs.next().unwrap(),
            m: attrs.next().unwrap(),
            a: attrs.next().unwrap(),
            s: attrs.next().unwrap()
        }
    };

    let parts = input.map(line_to_part);

    let run_part = |p: Part| {
        let mut workflow_name = "in";
        loop {
            let workflow = flows.get(workflow_name).unwrap();

            workflow_name = workflow.rules.iter()
                .filter(|r| apply_rule(&p, r))
                .map(|r| &r.destination)
                .next().unwrap_or(&workflow.default);

            if workflow_name == "A" {
                return p.x + p.m + p.a + p.s
            }
            if workflow_name == "R" {
                return 0
            }
        }
    };

    let result: u32 = parts.map(run_part).sum();

    println!("{:?}", result)
}
