use std::collections::HashMap;

/// Given a line from the input, return a Vector of tuples containing the numbers and their start and end indexes (start, end, number)
fn read_line_numbers(line: &str) -> Vec<(u32, u32, u32)> {
    for (idx, c) in line.chars().enumerate().filter(|(_, c)| c.is_ascii_digit()) {
        
    }
    Vec::new()
}

fn read_input(s: &str) -> (HashMap<u32, Vec<(u32, u32, u32)>>, Vec<(u32, u32)>) {
    // In this problem, everything that is not a `.` or a digit is considered a symbol
    let is_symbol = |x: char| !(x.is_ascii_digit() || x == '.');

    // Create a list of (x, y) coordinates of where symbols are located
    let symbols: Vec<(u32, u32)> = Vec::from_iter(
        // Process every line
        s.lines().enumerate().flat_map(|(n_line, line)|
            // Process every character
            line.chars().enumerate()
                .filter(|(_, c)| is_symbol(*c)) // Only get symbol characters
                .map(move |(idx, _)| (n_line as u32, idx as u32))
        )
    );

    let numbers: HashMap<u32, Vec<(u32, u32, u32)>> = HashMap::from_iter(
        s.lines().enumerate().map(|(n_line, line)|
            (n_line as u32, read_line_numbers(line))
        )
    );

    (numbers, symbols)
}

fn main() {
    let input = include_str!("../../inputs/03.txt");

    let (numbers, symbols) = read_input(input);

    println!("{:?} {:?}", numbers, symbols)
}
