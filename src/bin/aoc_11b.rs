use std::collections::HashMap;

const GALAXY_AGE: isize = 1000000;

static mut IDX: usize = 0;
fn gen_idx() -> usize {
    unsafe {
        IDX += 1;
        IDX
    }
}

fn main() {
    let input = include_str!("../../inputs/11.txt");

    let galaxies: HashMap<usize, (usize, usize)> = HashMap::from_iter(
        input.lines().enumerate()
            .filter(|(_, line)| line.contains('#'))
            .flat_map(|(line_n, line)|
                line.chars().enumerate()
                .filter(|(_, c)| *c == '#')
                .map(move |(col_n, _)| (gen_idx(), (line_n, col_n)))
            )
    );
    // println!("{:?}", galaxies);

    let lines = Vec::from_iter(
        input.lines()
        .map(|line| {if line.contains('#') {1} else {GALAXY_AGE}})
    );
    // println!("{:?}", lines);

    let mut cols = Vec::from_iter(
        input.lines().next().unwrap().chars()
            .map(|c| if c == '#' {1} else {GALAXY_AGE})
    );
    input.lines().for_each(|line| {
        line.chars().enumerate().for_each(|(idx, x)|
            if x == '#' {
                cols[idx] = 1
            }
        )
    });
    // println!("{:?}", cols);

    let result: isize = galaxies.iter().map(|(k, (x, y))|
        galaxies.iter().filter(move |(k2, _)| k > k2)
            .map(|(_, (x2, y2))|
                lines.iter().skip(*x.min(x2)).take(x.abs_diff(*x2)).sum::<isize>() +
                cols .iter().skip(*y.min(y2)).take(y.abs_diff(*y2)).sum::<isize>()
            ).sum::<isize>()
        ).sum();

    println!("{}", result);
}