type Direction = (isize, isize);
static UP:    Direction = (-1,  0);
static DOWN:  Direction = ( 1,  0);
static LEFT:  Direction = ( 0, -1);
static RIGHT: Direction = ( 0,  1);
static DEAD_END: (Direction, Direction) = ((-1,-1), (-1,-1));

fn letter_to_directions(x: &char) -> (Direction, Direction) {
    match x {
        '|' => (UP,   DOWN),
        'J' => (UP,   LEFT),
        'L' => (UP,   RIGHT),
        '7' => (DOWN, LEFT),
        'F' => (DOWN, RIGHT),
        '-' => (LEFT, RIGHT),
        '.' => ((0, 0), (0, 0)),
        _   => panic!()
    }
}

fn main() {
    let input = include_str!("../../inputs/10.txt");

    let matrix: Vec<Vec<_>> = input.lines()
        .map(|line| line.chars().collect())
        .collect();
    let matrix_lines = matrix.len() as isize;
    let matrix_colus = matrix.first().unwrap().len() as isize;

    let (start_x, line) = input.lines().enumerate()
        .filter(|(_, l)| l.contains('S'))
        .next().unwrap();
    let start_x = start_x as isize;
    let start_y = line.find('S').unwrap() as isize;

    let walk = |(x_, y_): &(isize, isize), (del_x, del_y): &Direction| {
        let x = x_ + del_x;
        let y = y_ + del_y;
        if  x < 0 ||
            y < 0 ||
            x >= matrix_lines ||
            y >= matrix_colus
        {
            return DEAD_END;
        }

        let pipe = matrix[x as usize][y as usize];

        let (dir1, dir2) = letter_to_directions(&pipe);
        if dir1 == (-del_x, -del_y) {
            return ((x, y), dir2);
        }
        if dir2 == (-del_x, -del_y) {
            return ((x, y), dir1);
        }
        DEAD_END
    };

    let mut ways = [
        ((start_x, start_y), UP),
        ((start_x, start_y), DOWN),
        ((start_x, start_y), LEFT),
        ((start_x, start_y), RIGHT)
    ].to_vec();

    let mut counter = 0;
    'outer: loop {
        ways.retain_mut(|(coord, dir)| {
            let x = walk(coord, dir);
            let dead_end = x == DEAD_END;
            if !dead_end {
                *coord = x.0;
                *dir = x.1;
            }
            return !dead_end
        });
        counter += 1;

        for (idx, (coord1, _)) in ways.iter().enumerate() {
            for (coord2, _) in ways[idx+1..].iter() {
                if coord1 == coord2 {
                    break 'outer;
                }
            }
        }
    };
    println!("{:?}", counter);
}
