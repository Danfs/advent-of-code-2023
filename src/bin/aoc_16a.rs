use std::iter::once;
use std::collections::VecDeque;

type Direction = (isize, isize);
const UP:    Direction = (-1,  0);
const DOWN:  Direction = ( 1,  0);
const LEFT:  Direction = ( 0, -1);
const RIGHT: Direction = ( 0,  1);

use std::iter::Iterator;
fn reflect(mirror: &char, (x, y): Direction) -> Box<dyn Iterator<Item = Direction>> {
    match (mirror, x, y) {
        ('-',  _, 0) => Box::new([LEFT, RIGHT].into_iter()),
        ('|',  0, _) => Box::new([UP, DOWN].into_iter()),
        ('/',  _, _) => Box::new(once((-y, -x))),
        ('\\', _, _) => Box::new(once((y, x))),
        ('|',  _, _) => Box::new(once((x, y))),
        ('.',  _, _) => Box::new(once((x, y))),
        ('-',  _, _) => Box::new(once((x, y))),
        _ => panic!()
    }
}

struct Dirs {
    up:    bool,
    down:  bool,
    left:  bool,
    right: bool
}

const fn empty_dirs() -> Dirs {
    Dirs { up: false, down: false, left: false, right: false }
}

fn main() {
    let input = include_str!("../../inputs/16.txt").lines()
        .map(|l| l.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();
    let lines = input.len() as isize;
    let cols = input[0].len() as isize;

    let is_inbounds = |x: isize, y: isize| {
        x >= 0 && x < lines && y >= 0 && y < cols
    };

    let mut aux: Vec<Vec<(bool, Dirs)>> = Vec::from_iter(
        (0..lines).map(|_| Vec::from_iter(
            (0..cols).map(|_| (false, empty_dirs()))
        ))
    );

    let mut paths: VecDeque<((isize, isize), Direction)> = VecDeque::from_iter(once(((0, 0), RIGHT)));

    while let Some(((x, y), dir)) = paths.pop_front() {
        if is_inbounds(x, y) {
            let aux = &mut aux[x as usize][y as usize];
            (*aux).0 = true;
            match dir {
                UP    => if aux.1.up    {continue} else {(*aux).1.up    = true},
                DOWN  => if aux.1.down  {continue} else {(*aux).1.down  = true},
                LEFT  => if aux.1.left  {continue} else {(*aux).1.left  = true},
                RIGHT => if aux.1.right {continue} else {(*aux).1.right = true},
                _ => panic!(),
            };

            let directions = reflect(&input[x as usize][y as usize], dir);
            for (del_x, del_y) in directions {
                paths.push_back(((x+del_x, y+del_y), (del_x, del_y)));
            }
        };
    }

    let result: usize = aux.iter().map(|v|
        v.iter().map(|(passed, _)| *passed as usize).sum::<usize>()
    ).sum();

    println!("{}", result);
}
