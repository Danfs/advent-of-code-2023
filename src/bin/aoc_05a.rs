use std::collections::BTreeMap;

fn main() {
    let mut lines = include_str!("../../inputs/05.txt").lines();
    let (_, seeds) = lines.next().unwrap().split_once(':').unwrap();

    let mut set1: BTreeMap<isize, bool> = BTreeMap::from_iter(seeds.trim().split(' ').map(|n| (n.parse::<isize>().unwrap(), false)));

    // The treatment for every group is the same, might as well use a loop with a break flag
    let mut flag = false;
    loop {
        // We create a new set, so we can control which numbers were already processed and which weren't
        let mut set2: BTreeMap<isize, bool> = BTreeMap::new();

        // Searches for the header of a map group, ignores the "header" line,
        // and reads lines until an empty line is found
        for line in lines.by_ref()
            .skip_while(|l| !l.ends_with(":"))
            .skip(1)
            .take_while(|l| !l.is_empty())
        {
            // Parses the line
            let (to, tail) = line.split_once(' ').unwrap();
            let (from, range) = tail.split_once(' ').unwrap();

            // Converts the strings to numbers, calculates the delta between `to` and `from`
            let to = to.parse::<isize>().unwrap();
            let from = from.parse::<isize>().unwrap();
            let range = range.parse::<isize>().unwrap();
            let diff: isize = to - from;

            // Get every element within the range, inserts the target value on the new set
            for (n, s) in set1.range_mut(from..(from+range)) {
                set2.insert(n+diff, false);
                *s = true;
            }
            flag = true;
        }
        // If the lines iterator was exhausted, the processing has finished
        if !flag {
            break;
        }
        // Every element that wasn't touched is passed without alteration to the new set
        for (n, _) in set1.iter().filter(|(_, b)| !**b) {
            set2.insert(*n, false);
        }

        // The new set is assigned as the working set
        set1 = set2;
        flag = false;
    }
    let (result, _) = set1.pop_first().unwrap();

    println!("{}", result);
}
