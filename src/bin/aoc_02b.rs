fn read_line(s: &str) -> u32 {
    let (_, results) = s.split_once(": ").unwrap();

    let mut red = 0;
    let mut blue = 0;
    let mut green = 0;

    for result in results.split("; ") {
        for ball in result.split(", ") {
            let (n, colour) = ball.split_once(' ').unwrap();
            let n: u32 = n.parse().unwrap();
            match colour {
                "red"   => red   = std::cmp::max(n, red),
                "green" => green = std::cmp::max(n, green),
                "blue"  => blue  = std::cmp::max(n, blue),
                _       => {}
            }
        }
    }
    red*green*blue
}

fn main() {
    let input = include_str!("../../inputs/02.txt");

    let result = input.lines().fold(0, |acc, x| acc+read_line(x));

    println!("{}", result)
}
