fn main() {
    let mut input = include_str!("../../inputs/06.txt").lines();

    let time = input.next().unwrap()
        .split_whitespace().skip(1)
        .collect::<String>()
        .parse::<usize>().unwrap();
    let dist = input.next().unwrap()
        .split_whitespace().skip(1)
        .collect::<String>()
        .parse::<usize>().unwrap();

    let mut upper = time/2;
    let mut lower = 1usize;

    // `upper` and `lower` stabilize at a difference of 1, which is why I need the +1
    while upper != (lower+1) {
        let half = (upper+lower)/2;
        if half * (time - half) > dist {
            upper = half;
        } else {
            lower = half;
        }
    }

    let result = time+1 - (2*upper);
    println!("{}", result);
}
