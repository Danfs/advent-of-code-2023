use std::{iter, cmp::Ordering, collections::HashMap};

type Grid = Vec<Vec<char>>;

fn roll_north(grid: &mut Grid) {
    let lines = grid.len();
    let colus = grid.first().unwrap().len();

    let mut vec = Vec::from_iter(iter::repeat(0usize).take(grid[0].len()));
    for line in 0..lines {
        for col in 0..colus {
            let c = grid[line][col];
            match c {
                '.' => {}
                'O' => {
                    grid[line][col] = '.';
                    let x = vec.get_mut(col).unwrap();
                    grid[*x][col] = 'O';
                    *x += 1;
                }
                '#' => {
                    *vec.get_mut(col).unwrap() = line+1;
                }
                _ => panic!(),
            };
        }
    }
}

fn roll_south(grid: &mut Grid) {
    let lines = grid.len();
    let colus = grid.first().unwrap().len();

    let mut vec = Vec::from_iter(iter::repeat(lines-1).take(colus));
    for line in (0..lines).rev() {
        for col in 0..colus {
            let c = grid[line][col];
            match c {
                '.' => {}
                'O' => {
                    grid[line][col] = '.';
                    let x = vec.get_mut(col).unwrap();
                    grid[*x][col] = 'O';
                    *x = x.saturating_sub(1);
                }
                '#' => {
                    *vec.get_mut(col).unwrap() = line.saturating_sub(1);
                }
                _ => panic!(),
            };
        }
    }
}

fn roll_west(grid: &mut Grid) {
    for line in grid.iter_mut() {
        let a = line.split(|c| c == &'#')
            .map(|seg| {
                let mut seg = seg.to_vec();
                seg.sort_by(|lhs, rhs| {
                    if lhs == rhs {
                        Ordering::Equal
                    } else if lhs == &'O' {
                        Ordering::Less
                    } else {
                        Ordering::Greater
                    }
                });
                seg.iter().collect::<String>()
            })
            .collect::<Vec<_>>()
            .join("#");
        *line = Vec::from_iter(a.chars());
    }
}


fn roll_east(grid: &mut Grid) {
    for line in grid.iter_mut() {
        let a = line.split(|c| c == &'#')
            .map(|seg| {
                let mut seg = seg.to_vec();
                seg.sort_by(|lhs, rhs| {
                    if lhs == rhs {
                        Ordering::Equal
                    } else if lhs == &'.' {
                        Ordering::Less
                    } else {
                        Ordering::Greater
                    }
                });
                seg.iter().collect::<String>()
            })
            .collect::<Vec<_>>()
            .join("#");
        *line = Vec::from_iter(a.chars());
    }
}

fn calculate_load(grid: &Grid) -> usize {
    let calculate_line_load = |weight: usize, line: &Vec<char>| {
        let amount = line.iter().filter(|c| **c == 'O').count();
        amount*weight
    };
    let len = grid.len();

    grid.iter().enumerate()
        .map(|(idx, line)| calculate_line_load(len-idx, line))
        .sum()
}

fn grid_to_key(grid: &Grid) -> String {
    grid.iter().flatten().collect::<String>()
}

fn main() {
    let mut input = include_str!("../../inputs/14.txt").lines()
        .map(|x| x.chars().collect::<Vec<_>>())
        .collect::<Vec<_>>();
    let input: &mut Vec<Vec<char>> = input.as_mut();

    let mut cache: HashMap<String, usize> = HashMap::new();

    let mut cycles_to_go = 0;
    for i in 1..1000000001 {
        roll_north(input);
        roll_west(input);
        roll_south(input);
        roll_east(input);

        let key = grid_to_key(input);
        if let Some(cycle_start) = cache.get(&key) {
            let cycle_size = i-cycle_start;
            cycles_to_go = (1000000000-cycle_start)%cycle_size;
            break;
        } else {
            cache.insert(key, i);
        }
    }

    for _ in 0..cycles_to_go {
        roll_north(input);
        roll_west(input);
        roll_south(input);
        roll_east(input);
    }

    let result = calculate_load(&input);
    println!("{}", result)
}
