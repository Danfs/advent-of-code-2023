use std::collections::HashMap;

fn main() {
    let mut input = include_str!("../../inputs/08.txt").lines();

    let guide = input.next().unwrap();

    let map: HashMap<&str, (&str, &str)> = HashMap::from_iter(input.skip(1)
        .filter_map(|x| x.split_once(" = "))
        .map(|(x, y)|
            (x, y.trim_start_matches('(').trim_end_matches(')').split_once(", ").unwrap()
        )));

    let mut count: usize = 0;
    let mut location = "AAA";
    for side in guide.chars().cycle() {
        if location == "ZZZ" {
            break;
        }

        if side == 'L' {
            location = map.get(location).unwrap().0;
        } else {
            location = map.get(location).unwrap().1;
        }
        count += 1;
    }
    println!("{}", count);
}
