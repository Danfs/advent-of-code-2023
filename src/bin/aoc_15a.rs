fn calc_hash(s: &str) -> u8 {
    s.as_bytes().iter().fold(0, |acc, x| acc.wrapping_add(*x).wrapping_mul(17))
}

fn main() {
    let input = include_str!("../../inputs/15.txt").trim();

    let result: usize = input.split(',').map(|s| calc_hash(s) as usize).sum();

    println!("{}", result)
}
