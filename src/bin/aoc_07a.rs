use std::collections::HashMap;
use std::cmp::Ordering;

static CARD_ORDER: [char; 13] = ['2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A'];

fn hand_score(s: &str) -> usize {
    let char_map = s.chars()
        .fold(HashMap::new(), |mut acc, c| {
            *acc.entry(c).or_insert(0) += 1;
            acc
        });

    match char_map.len() {
        1 => 7,
        2 => if char_map.into_values().fold(1, |acc, x| acc*x) == 4 {
                6
            } else {
                5
            }
        3 => if char_map.into_values().fold(1, |acc, x| acc*x) == 3 {
                4
            } else {
                3
            }
        4 => 2,
        5 => 1,
        _ => 0,
    }
}

fn comp_hand(lft: &str, rgt: &str) -> Ordering {
    let comp_card = |l: &char, r: &char| {
        let l_idx = CARD_ORDER.iter().position(|x| x == l).unwrap();
        let r_idx = CARD_ORDER.iter().position(|x| x == r).unwrap();
        l_idx.cmp(&r_idx)
    };
    let comp_equal_hand = |lft: &str, rgt: &str| {
        lft.chars().zip(rgt.chars())
            .map(|(l, r)| comp_card(&l, &r))
            .filter(|ord| ord.ne(&Ordering::Equal))
            .next().unwrap()
    };

    match hand_score(lft).cmp(&hand_score(rgt)) {
        Ordering::Equal => comp_equal_hand(lft, rgt),
        x => x,
    }
}

fn main() {
    let input = include_str!("../../inputs/07.txt");

    let mut cards_bids = input.lines()
        .filter_map(|x| x.split_once(' '))
        .map(|(h, b)| (h, b.parse::<usize>().unwrap()))
        .collect::<Vec<_>>();

    cards_bids.sort_unstable_by(|(hand1, _), (hand2, _)| comp_hand(hand1, hand2));

    let result: usize = cards_bids.iter().enumerate()
        .map(|(idx, (_, bid))| bid*(idx+1))
        .sum();
    println!("{}", result);
}
