use std::collections::{HashSet, HashMap};

fn read_line(line: &str) -> usize {
    let (_, numbers) = line.split_once(':').unwrap();
    let (prize_nums, my_nums) = numbers.split_once('|').unwrap();

    let my_nums:    HashSet<usize> = HashSet::from_iter(my_nums   .split(' ').filter_map(|x| x.parse::<usize>().ok()));
    let prize_nums: HashSet<usize> = HashSet::from_iter(prize_nums.split(' ').filter_map(|x| x.parse::<usize>().ok()));
    prize_nums.intersection(&my_nums).count()
}

fn main() {
    let input = include_str!("../../inputs/04.txt");

    let mut counter:HashMap<usize, usize> = HashMap::from_iter((0..input.lines().count()).map(|x| (x, 1)));

    input.lines().enumerate().for_each(|(idx, line)| {
        let matches = read_line(line);
        let n_card = counter.get(&idx).cloned().unwrap();
        ((idx+1)..=(idx+matches)).for_each(|i| {
            counter.entry(i).and_modify(|v| *v += n_card);
        });
    });
    println!("{}", counter.values().sum::<usize>());
}
